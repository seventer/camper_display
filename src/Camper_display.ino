/*
 * Test fo Nextion receive events
 * 0X65+Page ID+Component ID+TouchEvent+End+0XFF+0XFF+0XFF
 * bHup        65 01 06 01 FF FF FF 
 * bHdown      65 01 07 01 FF FF FF 
 * bMup        65 01 04 01 FF FF FF 
 * bMdown      65 01 05 01 FF FF FF 
 * b1DayUp     65 01 0C 01 FF FF FF 
 * b1DayDown   65 01 0D 01 FF FF FF 
 * b1MonUp     65 01 0E 01 FF FF FF 
 * b1MonDown   65 01 0F 01 FF FF FF 
 * b1YearUp    65 01 10 01 FF FF FF 
 * b1YearDown  65 01 11 01 FF FF FF 
 * bExit       65 01 08 01 FF FF FF 
 * 
 * xstr 150,80,150,30,3,YELLOW,1048,1,1,1,"31-12-1959"
 * Status 12V accu
 * 100%  12.7
 * 90% 12.5
 * 80% 12.42
 * 70% 12.32
 * 60% 12.2
 * 50% 12.06
 * 40% 11.9
 * 30% 11.75
 * 20% 11.58
 * 10% 11.31
 * 0%  10.5

*/

#define DEBUG

#include <DS3232RTC.h>      // https://github.com/JChristensen/DS3232RTC
#include <SoftwareSerial.h>
#include <DallasTemperature.h>
#include <OneWire.h>
#include <DHT.h>
#define DHT_DATA_PIN 4 // DHT22 sensor pin
#define ONE_WIRE_PIN 5 // Pin where dallase sensor is connected 
#define MAX_ATTACHED_DS18B20 1

#define BAT1_PIN  A0
#define BAT2_PIN  A3
#define MAXFAIL   5

#define POT_PIN   A1
int dimVal=64;

float vout = 0.0;
float vin = 0.0;
const float R1 = 33000.0; 
const float R2 = 10000.0; 


// Setup a oneWire instance to communicate with any OneWire devices 
OneWire oneWire(ONE_WIRE_PIN);
 
// Pass our oneWire reference to Dallas Temperature.
DallasTemperature dsSensors(&oneWire);


/*
 * Nextion objectID used in events
 */
#define P0_SETUP      0x04

#define P1_H_UP       0x06
#define P1_H_DOWN     0x07
#define P1_M_UP       0x04
#define P1_M_DOWN     0x05
#define P1_DAY_UP     0x0C
#define P1_DAY_DOWN   0x0D
#define P1_MON_UP     0x0E
#define P1_MON_DOWN   0x0F
#define P1_YEAR_UP    0x10
#define P1_YEAR_DOWN  0x11
#define P1_EXIT       0x08
#define P1_CANCEL     0x13
/*
 * Battery percentage pictureID
 */
#define PIC_0         0
#define PIC_10        1
#define PIC_20        2
#define PIC_30        3
#define PIC_40        4
#define PIC_50        5
#define PIC_60        6
#define PIC_70        7
#define PIC_80        8
#define PIC_90        9
#define PIC_100       10


/*
 * Nextion p0 & p1 objectName
 */
const String p0HH       ="t0HH";
const String p0MM       ="t0MM";
const String p0Date     ="t3";
const String p0TempIn   ="t0TempIn";
const String p0TempOut  ="t0TempOut";
const String p0Hum      ="t0Hum";
const String p0Acc1     ="t0Acc1";
const String p0Acc2     ="t0Acc2";
const String p0Batt1    ="p0Batt1";
const String p0Batt2    ="p0Batt2";

const String p1HH       ="t0HH";
const String p1MM       ="t0MM";
const String p1DAY      ="t1Day";
const String p1MON      ="t1Month";
const String p1YEAR     ="t1Year";

const String sMonth[]={"januari","februari","maart","april","mei","juni","juli","augustus","september","oktober","november","december"};

// Voltage percentage level
const float battLevel[]={10.5,11.31,11.58,11.75,11.9,12.06,12.2,12.32,12.42,12.5,12.7};
const int percPicID[]={0,1,2,3,4,5,6,7,8,9,10};

/*
 *  local vars for storing display vars
 */
int iHH=10;
int iMM=10;
int iD=31;
int iM=12;
int iY=18;
float tempIN=0;
int tempInFail=0;
float tempOut=0;
float humOut=0;

float battCar=12.8;
float battCmp=12.3;
float B1R1 = 100000.0; // resistance of R1 (100K) 
float B1R2 = 10000.0; // resistance of R2 (10K) 
float B2R1 = 100000.0; // resistance of R1 (100K) 
float B2R2 = 10000.0; // resistance of R2 (10K) 





/*
 * Helper vars for receive events from Nextion touch display
 */
boolean eventComplete = false;  // whether the string is complete
boolean inSetup = false;
int eventByteCount=0;
int eventCloseCount=0;
unsigned char pageID = 0;
unsigned char componentID = 0;
unsigned char touchEvent = 0;

/*
 * Timing loop vars
 */
const int ledPin =  LED_BUILTIN;// the number of the LED pin
int ledState = LOW;             // ledState used to set the LED
unsigned long previousMillis = 0;        // will store last time LED was updated
const long interval = 10000;           // interval for main loop in milliseconds

DHT dht;
SoftwareSerial mySerial(10, 11);

void setup() {
  // initialize serial:
  Serial.begin(9600);
  mySerial.begin(9600);
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);
  delay(dht.getMinimumSamplingPeriod());
  dht.setup(DHT_DATA_PIN); // set data pin of DHT sensor

  dsSensors.begin();
  
  setSyncProvider(RTC.get);   // the function to get the time from the RTC
    if(timeStatus() != timeSet)
        Serial.println("Unable to sync with the RTC");
    else
        Serial.println("RTC has set the system time");
#ifdef DEBUG
  Serial.println("Startup complete...");
#endif
  getTMVar();
  lcdDate(); // set date to p0
  // set dummy value to batt
  setLCD(p0Acc1,"----");
  setLCD(p0Acc2,"----");
  
  
}

/*
 * Main loop
 */
void loop() {
  String sTmp="";
  if (eventComplete) {
    processEvent();
  }



  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis;

// LDR routine for display brightness
    int ldrVal = analogRead(POT_PIN);
    int potVal = map(ldrVal, 0, 700, 64, 0);
#ifdef DEBUG      
       Serial.print("LDR="); Serial.print(ldrVal); Serial.print(" -> "); Serial.println(potVal);
#endif   
    if (potVal != dimVal) {
      dimVal = potVal;
      setDim(dimVal); 
     }

  
    if (inSetup==false) {
      getTMVar(); // Sync time vars with actual time
      sTmp = "";
      if (iHH<10) {
        sTmp.concat("0"); 
      }
      sTmp.concat(iHH);
      setLCD(p0HH,sTmp);
      
      sTmp = "";
      if (iMM<10) {
        sTmp.concat("0"); 
      }
      sTmp.concat(iMM);
      setLCD(p0MM,sTmp);
      
      updateDHT();
      
      tempIN = dsTemp();
      
      battCar = getBatt(BAT1_PIN);
      int percID = updBatt(&p0Acc1,battCar);
#ifdef DEBUG      
      Serial.print("Bat1="); Serial.print(battCar); Serial.print("-"); Serial.println(percID);
#endif      
      String ff = p0Batt1;
      ff.concat(".pic=");
      ff.concat(String(percID));
      mySerial.print(ff);
      endMessage();
      
      battCmp = getBatt(BAT2_PIN);
      percID = updBatt(&p0Acc2,battCmp);
#ifdef DEBUG      
      Serial.print("Bat2="); Serial.print(battCmp); Serial.print("-"); Serial.println(percID);
#endif      
      ff = p0Batt2;
      ff.concat(".pic=");
      ff.concat(String(percID));
      mySerial.print(ff);
      endMessage();    
      
      lcdDate();
    }
  }
}   // end main loop


float dsTemp() {
  dsSensors.requestTemperatures(); // Send the command to get temperatures
  float fTmp = dsSensors.getTempCByIndex(0);
#ifdef DEBUG
  Serial.print("DS1820 temp="); Serial.println(fTmp);
#endif  
  String sOut="";
  if ((fTmp < -50.0) || (fTmp > 84.0)) {
    tempInFail++;
    if (tempInFail>=MAXFAIL) {
      sOut="----";
    }
#ifdef DEBUG
  Serial.print("DS1820 invalid temperature. count="); Serial.println(tempInFail);
#endif       
  }
  else {
    tempInFail=0;
    char buffer[5];
    String sValue = dtostrf(fTmp, 2, 1, buffer);
    sOut.concat(sValue);
  }
  
  setLCD(p0TempIn,sOut); 
  return fTmp;
}



/*
 *  DHT temperature & moisture processing
 */
void updateDHT() {
  // Force reading sensor, so it works also after sleep()
    dht.readSensor(true);
    String tmpOut = "";
    
    // Get temperature from DHT library
    tempOut = dht.getTemperature();
    if (isnan(tempOut)) {
#ifdef DEBUG
      Serial.println("Failed reading temperature from DHT!");
#endif
      tmpOut.concat("----");
    } else  {
#ifdef DEBUG      
      Serial.print("Temperature from DHT="); Serial.println(tempOut);  
#endif      
      char buffer[5];
      String sValue = dtostrf(tempOut, 2, 1, buffer);
      tmpOut.concat(sValue);
    }
    setLCD(p0TempOut,tmpOut);
    
    tmpOut="";
    
   // Get humidity from DHT library
    float humidity = dht.getHumidity();
    if (isnan(humidity)) {
#ifdef DEBUG      
      Serial.println("Failed reading humidity from DHT");
#endif      
      tmpOut.concat("---");
    } else {
#ifdef DEBUG      
      Serial.print("Humidity from DHT="); Serial.println(humidity);  
#endif      
      tmpOut.concat(String(humidity));
    }
    setLCD(p0Hum,tmpOut);    
}



/*
 * return percentageID on voltage 
 */
int battPercentage(float voltage) {
  int iPercID=0;
  if(voltage>=battLevel[10]) {
    iPercID=10;
  }
  /*
  for (int i=10; i>1; i--) {
    if(voltage<=battLevel[i]) {
      iPercID=i-1;
    }  
  }
  */
  
  if(voltage<=battLevel[10]) {
    iPercID=9;
  }
  if(voltage<=battLevel[9]) {
    iPercID=8;
  }
  if(voltage<=battLevel[8]) {
    iPercID=7;
  }
  if(voltage<=battLevel[7]) {
    iPercID=6;
  }
  if(voltage<=battLevel[6]) {
    iPercID=5;
  }
  if(voltage<=battLevel[5]) {
    iPercID=4;
  }
  if(voltage<=battLevel[4]) {
    iPercID=3;
  }
  if(voltage<=battLevel[3]) {
    iPercID=2;
  }
  if(voltage<=battLevel[2]) {
    iPercID=1;
  }
  if(voltage<=battLevel[1]) {
    iPercID=0;
  }
  
  return iPercID;
}


int updBatt(String * batID, float voltage) {
  char buffer[5];
  String sVolt = dtostrf(voltage, 2, 1, buffer);
  int battPercId=battPercentage(voltage);
  setLCD(*batID,sVolt);
  return battPercId;
}

/*
   Voltmeter code
   value = analogRead(analogInput);
   vout = (value * 5.0) / 1024.0; // see text
   vin = vout / (R2/(R1+R2)); 
   if (vin<0.09) {
   vin=0.0;//statement to quash undesired reading !
 */
float getBatt(int battPort) {
  int val = analogRead(battPort);
  vout = (val * 5.0) / 1024.0; // see text
  vin = vout / (R2/(R1+R2)); 

  //val = map(val, 0, 1023, 100, 140);
  return  (float) vin;
}

/*
 * process Nextion event
 */
void processEvent() {
  String sTmp="";
#ifdef DEBUG    
    Serial.println("Event received from Nextion");
    Serial.print("pageID     :"); Serial.println(pageID,HEX);
    Serial.print("componentID:"); Serial.println(componentID,HEX);
    Serial.print("touchEvent :"); Serial.println(touchEvent,HEX);
#endif
    eventComplete = false;
    if (pageID==1) {
      switch(componentID) {
        case P1_H_UP:
          iHH++;
          if (iHH>23) {
            iHH=0;
          }
          sTmp = "";
          if (iHH<10) {
            sTmp.concat("0"); 
          }
          sTmp.concat(iHH);
          setLCD(p1HH,sTmp);
          break;
        case P1_H_DOWN:
          iHH--;
          if (iHH<0) {
            iHH=23;
          }
          sTmp = "";
          if (iHH<10) {
            sTmp.concat("0"); 
          }
          sTmp.concat(iHH);
          setLCD(p1HH,sTmp);
          break;
        case P1_M_UP:
          iMM++;
          if (iMM>59) {
            iMM=0;
          }
          sTmp = "";
          if (iMM<10) {
            sTmp.concat("0"); 
          }
          sTmp.concat(iMM);
          setLCD(p1MM,sTmp);
          break;
        case P1_M_DOWN:
          iMM--;
         if (iMM<0) {
            iMM=59;
          }
          sTmp = "";
          if (iMM<10) {
            sTmp.concat("0"); 
          }
          sTmp.concat(iMM);
          setLCD(p1MM,sTmp);
          break;
        case P1_DAY_UP:
          iD++;
          if (iD>31) {
            iD=1;
          }
          setLCD(p1DAY,String(iD));
          break;
        case P1_DAY_DOWN:
          iD--;
          if (iD<1) {
            iD=31;
          }
          setLCD(p1DAY,String(iD));
          break;
        case P1_MON_UP:
          iM++;
          if (iM>12) {
            iM=1;
          }
          setLCD(p1MON,String(iM));
          break;
        case P1_MON_DOWN:
          iM--;
          if (iM<1) {
            iM=12;
          }
          setLCD(p1MON,String(iM));
          break;
        case P1_YEAR_UP:
          iY++;
          setLCD(p1YEAR,String(iY));
          break;
        case P1_YEAR_DOWN:
          iY--;
          if (iY<18) {
            iY=18;
          }
          setLCD(p1YEAR,String(iY));
          break;                    
        case P1_EXIT:
          inSetup=false;
          saveRTC();
          previousMillis = 0;
          break;
        case P1_CANCEL:
          inSetup=false;
          previousMillis = 0;
          break;
      }
    }
    if (pageID==0 && componentID==P0_SETUP) {
      // Setup called
      inSetup=true;
      getTMVar();
      setLCD(p1HH,String(iHH));
      setLCD(p1MM,String(iMM));
      setLCD(p1DAY,String(iD));
      setLCD(p1MON,String(iM));
      setLCD(p1YEAR,String(iY));
    }
}



/*
 *  setLCD("t0HH",String(123));
 */
void setLCD(String objName, String objValue) {
  String sendThis = "";
  sendThis.concat(objName);
  sendThis.concat(".txt=\""); 
  sendThis.concat(objValue);
  sendThis.concat("\""); 
  writeString(sendThis); 
}


void setDim(int dimVal) {
  String sendThis = "";
  sendThis.concat("dim="); 
  sendThis.concat(String(dimVal));
  //sendThis.concat("\""); 
  writeString(sendThis); 
}




void writeString(String stringData) {
  for (int i = 0; i < stringData.length(); i++)
  {
    mySerial.write(stringData[i]);  
  }
  endMessage();
}


void saveRTC() {
  tmElements_t tm;
    tm.Hour = iHH;             
    tm.Minute = iMM;
    tm.Second = 0;
    tm.Day = iD;
    tm.Month = iM;
    tm.Year = iY + 30 ;
    setTime(iHH, iMM, 00, iD, iM, 2000+iY);    
    RTC.write(tm);         
}

void getTMVar() {
    iHH=hour();             
    iMM=minute();
    iD=day();
    iM=month();
    iY=year()-2000;
#ifdef DEBUG      
      Serial.print("TOY="); Serial.print(year()); Serial.print("-"); Serial.print(iM); Serial.print("-"); Serial.print(iD);
      Serial.print(" ");Serial.print(iHH);Serial.print(":");Serial.println(iMM);
#endif   

}

void lcdDate() {
  String sDate=String(day());
  sDate.concat(" ");
  sDate.concat(sMonth[month()-1]);
  sDate.concat(" ");
  sDate.concat(String(year()));
  setLCD(p0Date,sDate);
}



void endMessage() {
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.write(0xff);
}


/*
  0X65+Page ID+Component ID+TouchEvent+End+0XFF+0XFF+0XFF
*/
void serialEvent() {
  while (Serial.available()) {
    //Serial.print("*");
    // get the new byte:
    unsigned char inChar = (char)Serial.read();
    // add it to the inputString:
    
    Serial.print("-->eventByteCount="); Serial.print(eventByteCount); Serial.print("Char:"); Serial.println(inChar,HEX);
    
  switch (eventByteCount) {
    case 0:    // your hand is on the sensor
      if (inChar==0x65) {
        eventByteCount++;
        digitalWrite(ledPin, HIGH);
      }
      break;
    case 1:    // your hand is close to the sensor
      pageID=inChar;
      eventByteCount++;
      break;
    case 2:    // your hand is a few inches from the sensor
      componentID=inChar;
      eventByteCount++;
      break;
    case 3:    // your hand is nowhere near the sensor
      touchEvent=inChar;
      eventByteCount++;
      break;
  }
    
    if (inChar == 0xFF) {
      eventCloseCount++;
      //Serial.print("eventCloseCount:"); Serial.println(eventCloseCount);
      if (eventCloseCount==3) {
        eventCloseCount=0;
        eventByteCount=0;
        eventComplete = true;
        //delay(50);
        digitalWrite(ledPin, LOW);
      }
    }
  }
}
